package com.example.catsphotos.ui.screens

import android.util.Log
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.example.catsphotos.data.apiservice.CatsApi
import com.example.catsphotos.ui.model.CatsUIModel
import kotlinx.coroutines.flow.MutableStateFlow
import kotlinx.coroutines.flow.StateFlow
import kotlinx.coroutines.flow.asStateFlow
import kotlinx.coroutines.launch

class DetailViewModel: ViewModel() {

    private val _uiState = MutableStateFlow<CatsUIModel?>(null)
    val uiState: StateFlow<CatsUIModel?> = _uiState.asStateFlow()

    fun init(id: String) = viewModelScope.launch{
        val cat = CatsApi.retrofitService.getCat(id)
        Log.i("gatitos", id)
        val image = CatsApi.retrofitService.getCatImage(id)
        Log.i("gatitos", image.toString())

        _uiState.value = CatsUIModel(
            id = cat.id,
            imageUrl = image.first().imageUrl?:"",
            name = cat.name,
            temperament = cat.temperament,
            countryCode = cat.countryCode,
            description = cat.description,
            wikipedia_url = cat.wikipedia_url
        )
    }
}