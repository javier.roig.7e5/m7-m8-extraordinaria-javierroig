package com.example.catsphotos.ui.model.mapper

import com.example.catsphotos.data.apiservice.model.CatDto
import com.example.catsphotos.data.apiservice.model.CatImageDto
import com.example.catsphotos.ui.model.CatsUIModel

class CatsDtoUIModelMapper {

    fun map(breeds: List<CatDto>, images: List<CatImageDto>) : List<CatsUIModel> {
        return mutableListOf<CatsUIModel>().apply {
            breeds.forEachIndexed { index, catDto ->
                add(
                    CatsUIModel(
                        id = catDto.id,
                        imageUrl = images[index].imageUrl ?: "",
                        name = catDto.name,
                        temperament = catDto.temperament,
                        countryCode = catDto.countryCode,
                        description = catDto.description,
                        wikipedia_url = catDto.wikipedia_url
                    )
                )
            }
        }
    }
}