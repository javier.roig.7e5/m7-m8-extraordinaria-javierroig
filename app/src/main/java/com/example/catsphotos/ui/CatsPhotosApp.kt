/*
 * Copyright (C) 2022 The Android Open Source Project
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.example.catsphotos.ui

import android.content.Intent
import androidx.compose.animation.animateContentSize
import androidx.compose.animation.core.Spring
import androidx.compose.animation.core.spring
import androidx.compose.foundation.BorderStroke
import androidx.compose.foundation.Image
import androidx.compose.foundation.background
import androidx.compose.foundation.layout.*
import androidx.compose.foundation.lazy.LazyColumn
import androidx.compose.foundation.lazy.items
import androidx.compose.foundation.shape.RoundedCornerShape
import androidx.compose.material.*
import androidx.compose.material.icons.Icons
import androidx.compose.material.icons.filled.ArrowDropDown
import androidx.compose.material.icons.filled.ArrowDropUp
import androidx.compose.runtime.*
import androidx.compose.ui.Modifier
import androidx.compose.ui.draw.clip
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.layout.ContentScale
import androidx.compose.ui.platform.LocalContext
import androidx.compose.ui.res.stringResource
import androidx.compose.ui.text.font.FontWeight
import androidx.compose.ui.text.style.TextOverflow
import androidx.compose.ui.tooling.preview.Preview
import androidx.compose.ui.unit.dp
import androidx.compose.ui.unit.sp
import androidx.lifecycle.viewmodel.compose.viewModel
import coil.compose.rememberAsyncImagePainter
import com.example.catsphotos.DetailActivity
import com.example.catsphotos.ui.model.CatsUIModel
import com.example.catsphotos.ui.screens.CatsViewModel
import com.example.catsphotos.ui.theme.CatsPhotosTheme

@Composable
fun CatsPhotosApp(viewModel: CatsViewModel = viewModel()) {
    viewModel.getCatsPhotos()

    val uiState by viewModel.uiState.collectAsState()

    CatsPhotosTheme {
        uiState?.let { catList ->
            CatList(catList)
        }
    }
}

@Composable
fun CatList(catList: List<CatsUIModel>) {
    LazyColumn(
        modifier = Modifier
            .padding(15.dp)
            .fillMaxWidth()
            .background(Color.DarkGray)
    ) {
        items(catList) { item: CatsUIModel -> CatItem(catsUIModel = item) }
    }
}

@Composable
private fun CatItemButton(
    expanded: Boolean,
    onClick: () -> Unit,
) {
    IconButton(onClick = onClick) {
        Icon(
            imageVector = if (expanded) Icons.Filled.ArrowDropUp else Icons.Filled.ArrowDropDown,
            tint = Color.Blue,
            contentDescription = null
        )

    }
}

@Composable
fun CatItem(catsUIModel: CatsUIModel) {
    var expanded by remember { mutableStateOf(false) }
    Card(
        shape = RoundedCornerShape(15.dp),
        //border = BorderStroke(1.dp, Color.Blue),
        modifier = Modifier
            .padding(5.dp)
            .fillMaxSize()
            .background(Color.DarkGray)
    ) {
        Column(
            modifier = Modifier.animateContentSize(
                animationSpec = spring(
                    dampingRatio = Spring.DampingRatioMediumBouncy,
                    stiffness = Spring.StiffnessLow
                )
            )
                .background(Color.LightGray)
        ) {
            Row() {
                Image(
                    painter = rememberAsyncImagePainter(catsUIModel.imageUrl),
                    contentDescription = null,
                    modifier = Modifier.size(125.dp).weight(1f).clip(RoundedCornerShape(5)),
                    contentScale = ContentScale.Crop
                )
                Text(
                    text = catsUIModel.name,
                    modifier = Modifier
                        .padding(46.dp)
                        .weight(1f),
                    style = MaterialTheme.typography.h6
                )
                CatItemButton(expanded = expanded, onClick = { expanded = !expanded })
            }
            if (expanded) {
                CardDescription(catsUIModel)
            }
        }
    }
}

@Composable
fun CardDescription(catsUIModel: CatsUIModel, modifier: Modifier = Modifier,) {
    val context = LocalContext.current
    Row() {
        Column(
            modifier = modifier
                .padding(
                    start = 16.dp,
                    top = 8.dp,
                    bottom = 16.dp,
                    end = 16.dp
                )
                .weight(1f)
        ) {
            Text(
                text = catsUIModel.description,
                style = MaterialTheme.typography.body1,
                overflow = TextOverflow.Ellipsis,
                maxLines = 2,
                fontSize = 14.sp
            )
        }
        TextButton(modifier = Modifier.padding(top = 25.dp, end = 10.dp), onClick = {
            val intent = Intent(context, DetailActivity::class.java)
            intent.putExtra("id", catsUIModel.id)
            context.startActivity(intent)

        }) {
            Text(text = "See more", fontSize = 15.sp)
        }
    }
}

