package com.example.catsphotos.ui.model

data class CatsUIModel(
    val id: String,
    val imageUrl: String,
    val name: String = "",
    val temperament: String,
    val countryCode: String,
    val description: String,
    val wikipedia_url: String
)
