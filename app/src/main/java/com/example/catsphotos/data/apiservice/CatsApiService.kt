package com.example.catsphotos.data.apiservice

import com.example.catsphotos.data.apiservice.model.CatDto
import com.example.catsphotos.data.apiservice.model.CatImageDto
import retrofit2.Retrofit
import retrofit2.http.GET
import com.jakewharton.retrofit2.converter.kotlinx.serialization.asConverterFactory
import kotlinx.serialization.json.Json
import okhttp3.MediaType.Companion.toMediaType
import retrofit2.http.Path
import retrofit2.http.Query


private const val BASE_URL = "https://api.thecatapi.com/v1/"

private val retrofit = Retrofit.Builder()
    .addConverterFactory(Json{ignoreUnknownKeys = true}.asConverterFactory("application/json".toMediaType()))
    .baseUrl(BASE_URL)
    .build()

interface CatsApiService {
    @GET("./breeds")
    suspend fun getCats() : List<CatDto>

    @GET("./images/search")
    suspend fun getCatImage(@Query("breed_ids") id: String): List<CatImageDto>

    @GET("breeds/{breed_ids}")
    suspend fun getCat(@Path("breed_ids") breed_ids: String): CatDto
}

object CatsApi {
    val retrofitService : CatsApiService by lazy {
        retrofit.create(CatsApiService::class.java)
    }
}



