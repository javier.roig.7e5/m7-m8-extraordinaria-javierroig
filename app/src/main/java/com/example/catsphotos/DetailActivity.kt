package com.example.catsphotos

import android.os.Bundle
import androidx.activity.ComponentActivity
import androidx.activity.compose.setContent
import androidx.compose.foundation.Image
import androidx.compose.foundation.background
import androidx.compose.foundation.layout.*
import androidx.compose.foundation.rememberScrollState
import androidx.compose.foundation.shape.RoundedCornerShape
import androidx.compose.foundation.verticalScroll
import androidx.compose.material.Text
import androidx.compose.material.icons.filled.Phone
import androidx.compose.runtime.Composable
import androidx.compose.runtime.collectAsState
import androidx.compose.runtime.getValue
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.draw.clip
import androidx.compose.ui.draw.scale
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.platform.LocalContext
import androidx.compose.ui.text.font.FontFamily
import androidx.compose.ui.text.font.FontWeight
import androidx.compose.ui.text.style.TextAlign
import androidx.compose.ui.unit.dp
import androidx.compose.ui.unit.sp
import androidx.lifecycle.viewmodel.compose.viewModel
import coil.compose.rememberAsyncImagePainter
import com.example.catsphotos.ui.model.CatsUIModel
import com.example.catsphotos.ui.screens.DetailViewModel
import com.example.catsphotos.ui.theme.CatsPhotosTheme

class DetailActivity : ComponentActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        val id = intent.getStringExtra("id")
        setContent {
            id?.let { DetailApp(it) }
        }
    }

    @Composable
    fun DetailApp(id: String, viewModel: DetailViewModel = viewModel()) {
        viewModel.init(id)

        val uiState by viewModel.uiState.collectAsState()

        CatsPhotosTheme() {
            uiState?.let { catList ->
                CatSelected(catList)
            }
        }
    }

    @Composable
    fun CatSelected(uiState: CatsUIModel) {
        val context = LocalContext.current

        Column(
            horizontalAlignment = Alignment.CenterHorizontally,
            modifier = Modifier
                .background(Color.LightGray)
                .fillMaxSize()
        )
        {
            Image(
                painter = rememberAsyncImagePainter(uiState.imageUrl),
                contentDescription = null,
                modifier = Modifier
                    .padding(30.dp)
                    .scale(0.8f)
                    .clip(RoundedCornerShape(50))
            )
            Text(
                text = uiState.name,
                textAlign = TextAlign.Center,
                fontFamily = FontFamily.SansSerif,
                fontWeight = FontWeight.Bold,
                fontSize = 25.sp,
                modifier = Modifier.padding(bottom = 15.dp, start = 0.dp, end = 20.dp)
            )
            Text(
                text = uiState.wikipedia_url,
                fontSize = 10.sp,
                fontFamily = FontFamily.Serif,
                modifier = Modifier.padding(start = 25.dp, end = 25.dp)
            )
            Text(
                text = "Description:" + uiState.description,
                fontSize = 17.sp,
                fontFamily = FontFamily.SansSerif,
                modifier = Modifier.padding(start = 25.dp, end = 25.dp)
            )
            Text(
                text = "Country Code: " + uiState.countryCode,
                fontSize = 17.sp,
                fontFamily = FontFamily.SansSerif,
                modifier = Modifier.padding(top = 25.dp, bottom = 10.dp)
            )
            Text(
                text = "Temperament: " + uiState.temperament,
                fontSize = 17.sp,
                fontFamily = FontFamily.SansSerif,
                modifier = Modifier.padding(start = 25.dp, top = 15.dp, bottom = 20.dp, end = 25.dp)
            )
        }
    }
}